import character.*;
import java.util.ArrayList;

public class Game{
    ArrayList<Player> player = new ArrayList<Player>();
    
    /**
     * Fungsi untuk mencari karakter
     * @param String name nama karakter yang ingin dicari
     * @return Player chara object karakter yang dicari, return null apabila tidak ditemukan
     */
    public Player find(String name){
        for (Player i : player){
			if (i.getNama().equals(name)){
				return i;
			}
		}
		return null;
    }

    /**
     * fungsi untuk menambahkan karakter ke dalam game
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp){
        if(find(chara) == null){
		
			if(tipe.equals("Human")){
				Player temp = new Human(chara,hp);
				player.add(temp);
				
			}else if(tipe.equals("Magician")){
				Player temp = new Magician(chara,hp);
				player.add(temp);
				
			}else if(tipe.equals("Monster")){
				Player temp = new Monster(chara,hp);
				player.add(temp);
				
			}
			return chara + " Telah ditambahkan kedalam game";
		
		}else{
			return "Sudah ada karakter bernama " + chara;
			}
		
		
    }

    /**
     * fungsi untuk menambahkan karakter dengan tambahan teriakan roar, roar hanya bisa dilakukan oleh monster
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     * @param String roar teriakan dari karakter
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp, String roar){
        if(tipe.equals("Monster")){
			Player temp = new Monster(chara,hp,roar);
			player.add(temp);
			return chara + "ditambah ke dalam game";
		}else if(tipe.equals("Magician") || tipe.equals("Human")){
			return "Magician dan Human tidak bisa roar";
		}else if(find(chara).getNama().equals(chara)){
			return "Sudah ada karakter bernama " + chara;
		}else{
			return "Tidak ada " + tipe;
		}
		
		
		
    }

    /**
     * fungsi untuk menghapus character dari game
     * @param String chara character yang ingin dihapus
     * @return String result hasil keluaran dari game
     */
    public String remove(String chara){
        if(player.contains(find(chara))){
			player.remove(player.indexOf(find(chara)));
			return chara + " dihapus dari game";
		}else{
			return "Tidak ada " + chara ;
		}
		
    }


    /**
     * fungsi untuk menampilkan status character dari game
     * @param String chara character yang ingin ditampilkan statusnya
     * @return String result hasil keluaran dari game
     */
    public String status(String chara){
       if(find(chara) == null){
		   return "Tidak ada " + chara;
		}else if(find(chara).getMati() && find(chara).getDiet().isEmpty()){
		   return find(chara).getTipe() + " " + chara + "\n" + "HP: " + find(chara).getHp() + "\n" + "Sudah meninggal dunia dengan damai " + "\n" + "Belum memakan siapa siapa";
		}else if(find(chara).getMati()){
		   return find(chara).getTipe() + " " + chara + "\n" + "HP: " + find(chara).getHp() + " \n" + "Sudah meninggal dunia dengan damai" + "\n" + "memakan" + " " + diet(chara);
		}else if(find(chara).getDiet().isEmpty()){
		   return find(chara).getTipe() + " " + chara + "\n" + "HP: " + find(chara).getHp() + "\n" + "Masih Hidup " + "\n" + "Belum memakan siapa siapa";
		}else{
		   return find(chara).getTipe() + " " + chara + "\n" + "HP: " + find(chara).getHp() + "\n" + "Masih Hidup " + "\n" + "memakan" + " " + diet(chara);
		}
	  
    }

    /**
     * fungsi untuk menampilkan semua status dari character yang berada di dalam game
     * @return String result nama dari semua character, format sesuai dengan deskripsi soal atau contoh output
     */
    public String status(){
		String keluaran = "";
		for(int i = 0;i < player.size(); i++){
			keluaran += status(player.get(i).getNama()) + "\n" ;
		}
		return keluaran;
		
            
    }

    /**
     * fungsi untuk menampilkan character-character yang dimakan oleh chara
     * @param String chara Player yang ingin ditampilkan seluruh history player yang dimakan
     * @return String result hasil dari karakter yang dimakan oleh chara
     */
    public String diet(String chara){
		String keluaran = "";
        if(find(chara).getNama().equals(chara)){
			ArrayList<Player> lstDiet = find(chara).getDiet();
			for(int i = 0; i< lstDiet.size() ; i++){
				keluaran += lstDiet.get(i).getTipe() + " " + lstDiet.get(i).getNama();
			}
			return keluaran;
		}else{
			return "Tidak ada " + chara;
		}
		
		
		
    }

    /**
     * fungsi helper untuk memberikan list character yang dimakan dalam satu game
     * @return String result hasil dari karakter yang dimakan dalam 1 game
     */
    public String diet(){
		String keluaran = "";
        for(Player i : player){
			keluaran += diet(i.getNama()) + "\n";
			}			
		return "";
    }

    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di serang
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String attack(String meName, String enemyName){
        Player penyerang = find(meName);
		Player diserang = find(enemyName);
		if(find(meName)== null && find(enemyName) == null){
			return " Tidak ada " + meName + "dan" + enemyName;
		}else{
			return penyerang.attack(diserang);
		}
		
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. Method ini hanya boleh dilakukan oleh magician
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di bakar
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String burn(String meName, String enemyName){
		Player penyerang;
		Player dibakar;
		dibakar = find(enemyName);
		penyerang = find(meName);
		
        if(find(meName)== null && find(enemyName) == null){
			return " Tidak ada " + meName + "dan" + enemyName + "didalam game";
		}else if(find(meName).getTipe().equals("Human") || find(meName).getTipe().equals("Monster")){
			return meName + " tidak bisa melakukan burn";
		}else{
			Magician pembakar = (Magician)penyerang;
			return pembakar.burn(dibakar);
		}
		
		
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. enemy hanya bisa dimakan sesuai dengan deskripsi yang ada di soal
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di makan
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String eat(String meName, String enemyName){
		Player pemakan = find(meName);
		Player dimakan = find(enemyName);
        if(find(meName)== null && find(enemyName) == null){
			return " Tidak ada " + meName + "dan" + enemyName ;
		}else if(pemakan.canEat(pemakan,dimakan)){
			find(meName).setHp(find(meName).getHp() +15);
			pemakan.setDiet(dimakan);
			remove(dimakan.getNama());
			return meName + " memakan " + enemyName + "\n" + "Nyawa " + meName + " kini " + find(meName).getHp();
		}else{
			return meName + " Tidak bisa memakan " + enemyName; 
			
		}
    }

     /**
     * fungsi untuk berteriak. Hanya dapat dilakukan oleh monster.
     * @param String meName nama dari character yang akan berteriak
     * @return String result kembalian dari teriakan monster, format sesuai deskripsi soal
     */
    public String roar(String meName){
		if(find(meName) == null){
			return "Tidak ada " + meName;
		}
		else if(find(meName).getTipe().equals("Magician") || find(meName).getTipe().equals("Human")){
			return meName + " tidak bisa melakukan roar";
		}
		else{
			return find(meName).getRoar();
		}
		
    }
}





