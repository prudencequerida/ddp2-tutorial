package character;
import java.util.ArrayList;

//  write Player Class here
public class Player {
	protected String name;
	protected int hp = 0;
	protected ArrayList<Player> diet  = new ArrayList<Player>();
	protected String tipe;
	protected boolean mati;
	protected boolean bakar;
	protected String roar;
	
	public Player(String nama,int hp){
	this.name =nama;
	this.hp=hp;
	this.diet=diet;
	this.tipe=tipe;
	if(this.hp <=0){
		this.hp = 0;
		this.mati = true;
	}else{
		this.mati = false;
		}
	this.bakar = false;
	}
	
	public boolean getMati(){
		return this.mati;
	}
	public boolean getBakar(){
		return this.bakar;
	}
	public void setMati(){
		this.mati = true;
	}
	public void setBakar(){
		this.bakar = true;
	}
	
	public String getNama(){
		return this.name;
	}
	
	public void setNama(String name){
		this.name = name;
	}
	public int getHp(){
		return this.hp;
	}
	
	public String getRoar(){
		return this.roar;
	}
	public void setHp(int hp){
		this.hp = hp;
		if(this.hp <=0){
			this.hp = hp;
			this.mati = true;
		}
	}
	
	public void setTipe(String tipe){
		this.tipe=tipe;
	}
	
	public String getTipe(){
		return this.tipe;
	}
	
	public ArrayList<Player> getDiet(){
		return this.diet;
	}
	public void setDiet(Player korban){
		this.diet.add(korban);
	}
	public String attack(Player attacked){
		if(attacked.getTipe().equals("Magician")){
			attacked.setHp(attacked.getHp() - 20);
			return "Nyawa " + attacked.getNama() + " " + attacked.getHp();
		}else{
			attacked.setHp(attacked.getHp() - 10);
			return "Nyawa " + attacked.getNama() + " " + attacked.getHp();
		}
	}
	public boolean canEat(Player pemakan, Player dimakan){
		if (pemakan.getTipe().equals("Human") || pemakan.getTipe().equals("Magician")){
			if (dimakan.getTipe().equals("Monster") && dimakan.getMati() && dimakan.getBakar()){
				return true;
			}else{
				return false;
			}
		}else if(pemakan.getMati()){
			return false;
			
		}else{
			if(dimakan.getMati() || dimakan.getBakar()){
				return true;
			}else{
				return false;
			}
		}
	}	
	
		
		
	
	
	
	
	
}

