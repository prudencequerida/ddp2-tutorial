/**
 * BingoCard Template created by Nathaniel Nicholas
 * Template untuk mengerjakan soal bonus tutorial lab 5
 * Template ini tidak wajib digunakan
 * Side Note : Jangan lupa untuk membuat class baru yang memiliki method main untuk menjalankan program dengan spesifikasi yang diharapkan
 */
import java.util.Scanner;

public class BingoCard {

	private Number[][] numbers;
	private Number[] numberStates; 
	private boolean isBingo;
	
	public BingoCard(Number[][] numbers, Number[] numberStates) {
		this.numbers = numbers;
		this.numberStates = numberStates;
		this.isBingo = false;
	}
	
	public static void main (String args[]){
		Scanner input = new Scanner(System.in);
		Number[][] numbers = new Number [5][5];
		Number [] numberStates = new Number [100];
		BingoCard player = new BingoCard (numbers,numberStates);
		
		
		for (int a = 0 ; a < 5 ; a++){
			for (int b = 0 ; b < 5 ; b++){
				int angka1 = input.nextInt();
				player.numbers[a][b] = new Number(angka1, a, b);
				player.numberStates[angka1] = player.numbers[a][b];
			}
			input.nextLine();
			
		}
		
		while(true){
			String[] inputperintah = input.nextLine().split(" ");
			switch (inputperintah[0].toUpperCase()){
				case "MARK" :
					System.out.println(player.markNum(Integer.parseInt(inputperintah[1])));
					break;
				case "INFO":
					System.out.println(player.info());
					break;
				case "RESTART":
					player.restart();
					break;
				default:
					System.out.println("Incorrect Command");
			}
			if (player.isBingo()){
				System.out.println("BINGO!");
				System.out.println(player.info());
			}
		}
	}
					
	

	public Number[][] getNumbers() {
		return numbers;
	}

	public void setNumbers(Number[][] numbers) {
		this.numbers = numbers;
	}

	public Number[] getNumberStates() {
		return numberStates;
	}

	public void setNumberStates(Number[] numberStates) {
		this.numberStates = numberStates;
	}	

	public boolean isBingo() {
		return isBingo;
	}

	public void setBingo(boolean isBingo) {
		this.isBingo = isBingo;
	}

	public String markNum(int num){
		try{
			if(getNumberStates()[num].isChecked()){
				return num + " sudah tersilang sebelumnya" ;
			}
			else{
				getNumberStates()[num].setChecked(true);
				for (int a = 0; a < 5; a++){
					if(this.numbers[a][0].isChecked() && this.numbers[a][1].isChecked() &&
					   this.numbers[a][2].isChecked() && this.numbers[a][3].isChecked() &&
					   this.numbers[a][4].isChecked()){
					   this.isBingo = true;
					   break;
					}
					if(this.numbers[0][a].isChecked() && this.numbers[1][a].isChecked() &&
					   this.numbers[2][a].isChecked() && this.numbers[3][a].isChecked() &&
					   this.numbers[4][a].isChecked()){
					   this.isBingo = true;
					   break;
					}
					if(this.numbers[0][4].isChecked() && this.numbers[1][3].isChecked() &&
					   this.numbers[2][2].isChecked() && this.numbers[3][1].isChecked() &&
					   this.numbers[4][0].isChecked()){
					   this.isBingo = true;
					   break;
					}
					if(this.numbers[0][0].isChecked() && this.numbers[1][1].isChecked() &&
					   this.numbers[2][2].isChecked() && this.numbers[3][3].isChecked() &&
					   this.numbers[4][4].isChecked()){
					   this.isBingo = true;
					   break;
					}
				}
			}
			return num + "tersilang";
	}
	catch (NullPointerException e) {
		return "kartu tidak memiliki angka " + num;
	}
}
	
	public String info(){
		String output = "";
		for (int b = 0; b < 5; b++){
			for (int a = 0; a < 5; a++){
				output += "| ";
				if (getNumbers()[b][a].isChecked() == true){
					output += "X ";
				}else {
					output += getNumbers()[b][a].getValue();
				}
				output += " ";
			}
			output += "|\n";
		}
		return output;
	}
	
	public void restart(){
		for (int i = 0; i < 5; i++){
			for (int j = 0; j < 5; j++){
				getNumbers()[i][j].setChecked(false);
			}
		}
		System.out.println("Mulligan!");
	}
	

}
