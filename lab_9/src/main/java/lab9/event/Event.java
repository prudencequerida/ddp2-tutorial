package lab9.event;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

/**
* A class representing an event and its properties
*/
public class Event implements Comparable<Event>
{
    /** Name of event */
    private String nama;
    
    // TODO: Make instance variables for representing beginning and end time of event
    private GregorianCalendar wktmulai;
	private GregorianCalendar wktakhir;

    // TODO: Make instance variable for cost per hour
    private BigInteger biaya;
    // TODO: Create constructor for Event class
    public Event(String nama, GregorianCalendar wktmulai, GregorianCalendar wktakhir, BigInteger biaya){
		this.nama = nama;
		this.wktmulai = wktmulai;
		this.wktakhir = wktakhir;
		this.biaya = biaya;
	}
    /**
    * Accessor for name field. 
    * @return name of this event instance
    */
    public String getName() {return this.nama;}
	public BigInteger getCost(){return biaya;}
	
    
    // TODO: Implement toString()
	public String toString() {
		SimpleDateFormat tanggal = new SimpleDateFormat("dd-MM-yyyy, HH:mm:ss");
		return this.nama +  "\nWaktu mulai: " + tanggal.format(this.wktmulai.getTime()) +
							"\nWaktu selesai: " + tanggal.format(this.wktakhir.getTime()) +
							"\nBiaya kehadiran: " + biaya.toString();
	}
	
	public boolean isCompatible(Event other){
		if (wktmulai.before(other.wktmulai) && wktakhir.before(other.wktmulai)){
			return true;
		}
		else if(other.wktmulai.before(wktmulai) && other.wktakhir.before(wktmulai)){
			return true;
		}
		else if(wktakhir.compareTo(other.wktmulai) == 0 || other.wktakhir.compareTo(wktmulai) == 0){
			return true;
		}
		return false;
	}
	
	public int compareTo(Event event){
		return wktmulai.compareTo(event.wktmulai);
	}
	
	public Event cloneEvent(){
		Event temp = new Event(this.nama, this.wktmulai , this.wktakhir, this.biaya);
		return temp;
    
    // HINT: Implement a method to test if this event overlaps with another event
    //       (e.g. boolean overlapsWith(Event other)). This may (or may not) help
    //       with other parts of the implementation.
	}

}


