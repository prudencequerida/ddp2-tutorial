package movie;

public class Movie {
	
	private String judul;
	private String genre;
	private int durasi;
	private String rating;
	private String jenis;
	private int batasumur;
	
	public Movie ( String judul, String rating, int durasi,String genre, String jenis){
		this.judul = judul;
		this.genre = genre;
		this.durasi = durasi;
		this.rating = rating;
		this.jenis = jenis;
		
		if(this.rating == "UMUM"){
			this.batasumur = 0;
		}
		else if(this.rating == "REMAJA"){
			this.batasumur = 13;
		}
		else if(this.rating == "DEWASA"){
			this.batasumur = 17;
		}
	}

	public String getJudul(){
		return judul;
	}
	
	public void setJudul(String judul){
		this.judul = judul;
	}
	
	public String getGenre(){
		return genre;
	}

	public void setGenre(String genre){
		this.genre = genre;
	}
	
	public int getDurasi(){
		return durasi;
	}
	
	public void setDurasi(int durasi){
		this.durasi = durasi;
	}
	
	public String getRating(){
		return rating;
	}
	
	public void setRating(String rating){
		this.rating = rating;
	}
	
	public String getJenis(){
		return jenis;
	}
	
	public void setJenis(String jenis){
		this.jenis = jenis;
	}
	
	public int getBatasumur(){
		return batasumur;
	}
	
	public void setBatasumur(int batasumur){
		this.batasumur = batasumur;
	}
	
	public String toString(){
		return "Judul \t :" + judul + "\n" + "Genre \t :" + genre + "\n" + "Durasi \t :" + durasi + "menit" + "\n"  + 
				"Rating \t :" + rating + "\n" + "Jenis \t :" + jenis;
	}
	
}

