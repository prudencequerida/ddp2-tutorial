package lab_1;
// Prudence Querida

public class CheckAge {

    public static void main(String[] args) {
        int age = 19;

        if (age < 10){
            System.out.println("KIDS");
        }else if (age < 20){
            System.out.println("TEEN");
        }else if (age < 50){
            System.out.println("ADULT");
        }else {
            System.out.println("SENIOR");
        }
    }
}
