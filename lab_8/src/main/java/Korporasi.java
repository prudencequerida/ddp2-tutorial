import java.util.ArrayList;

public class Korporasi{
	ArrayList<Karyawan> karyawan = new ArrayList<Karyawan>();
	int gajiStaff = 18000;

	public void setGajiStaff(int gaji){
		gajiStaff = gaji;
	}
	public int getGajiStaff(){
		return gajiStaff;
	}
	
	public Karyawan find(String nama){
		for(Karyawan i : karyawan){
			if(i.getNama().equals(nama)){
				return i;
			}
		}
		return null;
	}
	
	public String add(String tipe, String nama, int gaji){
		if(find(nama) == null){
			if(tipe.equals("MANAGER")){
				Karyawan temp = new Manager(nama,gaji);
				karyawan.add(temp);
			}else if(tipe.equals("STAFF")){
				Karyawan temp = new Staff(nama,gaji);
				karyawan.add(temp);
			}else if(tipe.equals("INTERN")){
				Karyawan temp = new Intern(nama,gaji);
				karyawan.add(temp);
			}
			return nama + " mulai bekerja sebagai " + tipe + " di PT. TAMPAN";
		}else{
			return "Karyawan dengan nama " + nama + " telah terdaftar";
		}
	}	
	public String status(String nama){
		if(find(nama) == null){
			return "Karyawan tidak ditemukan";
		}else{
			Karyawan namaKaryawan = find(nama);
			return nama + " " + namaKaryawan.getGaji();
		}
	}
	
	public String rekrutBawahan(String membawahi, String dibawahi){
		if(find(membawahi) == null || find(dibawahi) == null){
			return "Nama tidak berhasil ditemukan";
		}else{
			Karyawan boss = find(membawahi);
			Karyawan bawahan = find(dibawahi);
			if(boss.canBoss(boss,bawahan) == false){
				return "Anda tidak layak memiliki bawahan";
			}else{
				boss.setBawahan(bawahan);
				return "Karyawan " + dibawahi + " telah menjadi bawahan " + membawahi;
			}
		}
	}
	public String gajian(){
		String keluaran = "";
		for(Karyawan i : karyawan){
			i.setJumlahgajian(i.getJumlahgajian() + 1);
			if(i.getJumlahgajian() == 6){
				int temp = i.getGaji();
				i.setGaji(i.getGaji() + i.getGaji()/10);
				keluaran += "\n" + i.getNama() + " mengalami kenaikan gaji sebesar 10% dari " + temp + " menjadi " + i.getGaji()  ; 	
			}else if(i.getTipe().equals("STAFF") && i.getGaji() >= getGajiStaff()){
				i.setTipe("MANAGER");
				keluaran += "\n" + "Selamat, " + i.getNama() + " telah dipromosikan menjadi MANAGER" ;
			}
		
		}
		return "Semua karyawan telah diberikan gaji" +  keluaran;
	}
}	