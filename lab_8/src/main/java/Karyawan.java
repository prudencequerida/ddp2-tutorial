import java.util.ArrayList;
public class Karyawan{
	protected String nama;
	protected String tipe;
	protected int gaji;
	protected ArrayList<Karyawan> bawahan = new ArrayList<Karyawan>();
	protected int jumlahGajian;

	public Karyawan(String nama, int gaji){
		this.nama = nama;
		this.gaji = gaji;
		this.jumlahGajian = jumlahGajian;
	}

	public void setNama(String nama){
		this.nama = nama;
	}
	public void setGaji(int gaji){
		this.gaji = gaji;
	}
	public void setTipe(String tipe){
		this.gaji = gaji;
	}
	public void setBawahan(Karyawan subordinate){
		this.bawahan.add(subordinate);
	}
	public void setJumlahgajian(int jumlah){
		this.jumlahGajian = jumlah;
		if(this.jumlahGajian == 7){
			this.jumlahGajian = 1;
		}
	}

	public String getNama(){
		return this.nama;
	}
	public int getGaji(){
		return this.gaji;
	}
	public String getTipe(){
		return this.tipe;
	}
	public ArrayList<Karyawan> getBawahan(){
		return this.bawahan;
	}
	public int getJumlahgajian(){
		return this.jumlahGajian;
	}
	public int sizeBawahan(){
		return this.bawahan.size();
	}

	public boolean canBoss(Karyawan atasan, Karyawan dibawahi){
		if(atasan.sizeBawahan() == 10){
			return false;
		}
		else if(atasan.getTipe().equals("Manager")){
			if(dibawahi.getTipe().equals("Staff") || dibawahi.getTipe().equals("Intern")){
				return true;
			}
			else{
				return false;
			}
		}
		else if(atasan.getTipe().equals("Staff")){
			if(dibawahi.getTipe().equals("Intern")){
				return true;
			}
			else{
				return false;
			}
		}
		else{
			return false;
		}
	}

}