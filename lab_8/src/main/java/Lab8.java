import java.util.Scanner;

class Lab8 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        Korporasi ptTampan = new Korporasi();

        int perintah = sc.nextInt();
        sc.nextLine();
		ptTampan.setGajiStaff(perintah);
        
        while(true){
            String command = sc.nextLine();
            
            String[] commandList = command.split(" ");
            
            if(commandList[0].equalsIgnoreCase("TAMBAH_KARYAWAN")){
                System.out.println(ptTampan.add(commandList[2],commandList[1],Integer.parseInt(commandList[3])));
            
            }else if(commandList[0].equalsIgnoreCase("STATUS")){
                System.out.println(ptTampan.status(commandList[1]));
            
            }else if(commandList[0].equalsIgnoreCase("TAMBAH_BAWAHAN")){
                System.out.println(ptTampan.rekrutBawahan(commandList[1],commandList[2]));
            
            }else if(commandList[0].equalsIgnoreCase("GAJIAN")){
                System.out.println(ptTampan.gajian());
            
            }else{
                System.out.println("Input Salah");
            }
        }
    }
}